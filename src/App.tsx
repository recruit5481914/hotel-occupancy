import React from 'react';
import './App.css';
import {
		Form,
		ValueChangeParams
} from './components/Form';
import { Result } from './components/Result';

function App() {
		const [rooms, setRooms] = React.useState<ValueChangeParams>({
				premiumRooms: [],
				economyRooms: [],
		});

		const onRoomsChange = (val: ValueChangeParams) => {
				setRooms(val)
		};

		return (
				<main>
				    <Form onValueChange={onRoomsChange} />

				    <Result rooms={rooms} />
				</main>
		);
}

export default App;
