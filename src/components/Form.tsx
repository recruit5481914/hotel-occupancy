import {
		FormEvent,
} from 'react';
import { assignGuestRooms } from '../helpers/assignGuestRooms';
import guestsData from '../data/guests.json';

export type ValueChangeParams = {
		premiumRooms: number[];
		economyRooms: number[];
}

type Props = {
		readonly onValueChange: (val: ValueChangeParams) => void;
}

export function Form({ onValueChange }: Props) {
		const onSubmit = (e: FormEvent<HTMLFormElement>) => {
				e.preventDefault();
				e.stopPropagation();

				const formInputs = e.target as typeof e.target & {
						premium: {
								value: string;
						},
						economy: {
								value: string;
						}
				};

				const result = assignGuestRooms({
						premium: Number(formInputs.premium.value),
						economy: Number(formInputs.economy.value),
				}, guestsData)

				onValueChange(result);
		};

		return (
				<form onSubmit={onSubmit}>
						<label>
								Premium rooms
						</label>
						<input type="number" id="premium" />

						<label>
								Economy rooms
						</label>
						<input type="number" id="economy" />

						<button type="submit">
								Submit
						</button>
				</form>
		)
}