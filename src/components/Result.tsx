import { ValueChangeParams } from './Form';

export function calculateTotalEarnings(roomsArr: number[]) {
		return roomsArr.length ? roomsArr.reduce((prevVal, currVal) => prevVal + currVal) : 0;
}

type Props = {
		readonly rooms: ValueChangeParams;
}

export function Result({ rooms }: Props) {
		return (
				<div>
						<h2>
								Usage premium
						</h2>
						<p>
							Rooms used: { rooms.premiumRooms.length }
						</p>
						<p>
								Total earnings: {calculateTotalEarnings(rooms.premiumRooms)}
						</p>

						<h2>
								Usage economy
						</h2>
						<p>
								Rooms used: { rooms.economyRooms.length }
						</p>
						<p>
								Total earnings: {calculateTotalEarnings(rooms.economyRooms)}
						</p>
				</div>
		)
}