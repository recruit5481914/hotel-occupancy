import { assignGuestRooms } from './assignGuestRooms';
import guests from '../data/guests.json';
import { calculateTotalEarnings } from '../components/Result';

test('assignGuestRooms should return object with premium and free rooms', () => {
	expect(assignGuestRooms({
			premium: 0,
			economy: 0,
	}, [])).toEqual({
			premiumRooms: [],
			economyRooms: [],
	});
});

test('assignGuestRooms should assign guests to all rooms if guests number is equal or higher than number of rooms', () => {
		const premiumRooms = 3;
		const economyRooms = 3;

		const res = assignGuestRooms({
				premium: premiumRooms,
				economy: economyRooms,
		}, guests);
		expect(res.premiumRooms.length).toEqual(premiumRooms);
		expect(res.economyRooms.length).toEqual(economyRooms);
});

test('assignGuestRooms should assign economy customers to premium rooms if no economy rooms are left and premium rooms are available', () => {
		const mockData = [200, 220, 58, 98, 42, 55, 45, 80, 70];
		const premiumRooms = 3;
		const economyRooms = 5;

		const res = assignGuestRooms({
				premium: premiumRooms,
				economy: economyRooms,
		}, mockData);

		expect(res.premiumRooms.length).toEqual(premiumRooms);
		expect(res.economyRooms.length).toEqual(economyRooms);
});

test('should not assign economy rooms to customers paying 100 or above', () => {
		const premiumRooms = 3;
		const economyRooms = 3;

		const res = assignGuestRooms({
				premium: premiumRooms,
				economy: economyRooms,
		}, guests);

		const totalPremium = calculateTotalEarnings(res.premiumRooms);
		const totalEconomy = calculateTotalEarnings(res.economyRooms);

		const arePremiumRoomsOverLimit = res.premiumRooms.every((room) => room >= 100);
		const areEconomyRoomsUnderLimit = res.economyRooms.every((room) => room < 100);
		expect(arePremiumRoomsOverLimit).toBeTruthy();
		expect(areEconomyRoomsUnderLimit).toBeTruthy();

		expect(res.economyRooms.length).toEqual(economyRooms);
		expect(res.premiumRooms.length).toEqual(premiumRooms);
		expect(totalEconomy).toEqual(167);
		expect(totalPremium).toEqual(738);
});

test('Provided Test case 2', () => {
		const premiumRooms = 7;
		const economyRooms = 5;

		const res = assignGuestRooms({
				premium: premiumRooms,
				economy: economyRooms,
		}, guests);

		const totalPremium = calculateTotalEarnings(res.premiumRooms);
		const totalEconomy = calculateTotalEarnings(res.economyRooms);

		expect(res.economyRooms.length).toEqual(4);
		expect(res.premiumRooms.length).toEqual(6);
		expect(totalEconomy).toEqual(189);
		expect(totalPremium).toEqual(1054);
});

test('Provided test case 3', () => {
		const premiumRooms = 2;
		const economyRooms = 7;

		const res = assignGuestRooms({
				premium: premiumRooms,
				economy: economyRooms,
		}, guests);

		const totalPremium = calculateTotalEarnings(res.premiumRooms);
		const totalEconomy = calculateTotalEarnings(res.economyRooms);

		expect(res.economyRooms.length).toEqual(4);
		expect(res.premiumRooms.length).toEqual(2);
		expect(totalEconomy).toEqual(189);
		expect(totalPremium).toEqual(583);
});

test('Provided test case 4', () => {
		const premiumRooms = 7;
		const economyRooms = 1;

		const res = assignGuestRooms({
				premium: premiumRooms,
				economy: economyRooms,
		}, guests);

		const totalPremium = res.premiumRooms.reduce((prevVal, currVal) => prevVal + currVal);
		const totalEconomy = res.economyRooms.reduce((prevVal, currVal) => prevVal + currVal)

		expect(res.economyRooms.length).toEqual(1);
		expect(res.premiumRooms.length).toEqual(7);
		expect(totalEconomy).toEqual(45);
		expect(totalPremium).toEqual(1153);
});