type AvailableRooms = {
		readonly premium: number;
		readonly economy: number;
}

type AssignedRooms = {
		readonly premiumRooms: number[];
		readonly economyRooms: number[];
}

function sortHighestFirst(a: number, b: number) {
		return b - a;
}

function sortGuestsIntoEconomyAndPremium(guestPrice: number[]) {
		const sortedGuests = guestPrice.sort(sortHighestFirst);
		const premiumGuests: number[] = [];
		const economyGuests: number[] = [];

		sortedGuests.forEach((price) => {
				if (price >= 100) {
						premiumGuests.push(price);
				} else {
						economyGuests.push(price);
				}
		});

		return {
				premiumGuests,
				economyGuests,
		}
}

function assignPremiumRooms(premiumRoomsAvailable: number, premiumGuestPrices: number[]) {
		let freePremiumRooms = premiumRoomsAvailable;
		const premium: number[] = [];

		for (let i = 0; i < premiumGuestPrices.length; i++) {
				if (premiumRoomsAvailable < i + 1) {
						break;
				}
				premium.push(premiumGuestPrices[i]);
				freePremiumRooms--;
		}

		return {
				premiumRooms: premium,
				freePremiumRooms,
		};
}

function assignEconomyGuests(economyRoomsAvailable: number, economyGuests: number[], freePremiumRooms: number, premiumRooms: number[]) {
		const economy: number[] = [];

		const canPlaceAllGuestsInEconomy = economyRoomsAvailable >= economyGuests.length + 1;
		const canPlaceGuestsInPremium = freePremiumRooms > 0;

		const guestsWithoutRoom = !canPlaceAllGuestsInEconomy && canPlaceGuestsInPremium ? economyGuests.length + 1 - economyRoomsAvailable : 0;
		const guestsMovedToPremium = guestsWithoutRoom > freePremiumRooms ? freePremiumRooms : guestsWithoutRoom;

		for (let i = guestsMovedToPremium; i < economyGuests.length; i++) {
				const canPlaceInEconomy = economyRoomsAvailable + guestsMovedToPremium > i;
				if (!canPlaceInEconomy) {
						break;
				}

				economy.push(economyGuests[i]);
		}

		if (guestsMovedToPremium > 0) {
				for (let i = 0; i < guestsMovedToPremium; i++) {
						premiumRooms.push(economyGuests[i]);
				}
		}


		return {
				economyRooms: economy,
		};
}

export function assignGuestRooms(availableRooms: AvailableRooms, guestPrice: number[]): AssignedRooms {
		const { premiumGuests, economyGuests } = sortGuestsIntoEconomyAndPremium(guestPrice);
		const { premium: premiumRoomsAvailable, economy: economyRoomsAvailable } = availableRooms;

		const {
				freePremiumRooms,
				premiumRooms
		} = assignPremiumRooms(premiumRoomsAvailable, premiumGuests);

		let {
				economyRooms
		} = assignEconomyGuests(economyRoomsAvailable, economyGuests, freePremiumRooms, premiumRooms);

		return {
				premiumRooms,
				economyRooms,
		};
}